extends Node

var levels = [
	"res://levels/level_1.tscn",
	"res://levels/level_2.tscn"
]

var current_level = -1;

func _process(delta):
	if (Input.is_action_pressed("QUIT_GAME")): 
		get_tree().quit()

func next_level():
	current_level = current_level + 1;
	if (current_level < levels.size()) :
		get_tree().change_scene(levels[current_level]);
	else :
		get_tree().change_scene("res://menu.tscn");
		current_level = -1;
