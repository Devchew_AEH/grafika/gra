extends KinematicBody2D

var playerMovement = 120 # to mozna zmieniac 

func _physics_process(delta): 
	if (Input.is_action_pressed("PLAYER_UP")): 
		move_and_collide(Vector2(0, -playerMovement*delta)) 
	if (Input.is_action_pressed("PLAYER_DOWN")): 
		move_and_collide(Vector2(0, playerMovement*delta)) 
	if (Input.is_action_pressed("PLAYER_RIGHT")):
		get_node( "Player" ).set_flip_h( false );
		move_and_collide(Vector2(playerMovement*delta, 0)) 
	if (Input.is_action_pressed("PLAYER_LEFT")): 
		get_node( "Player" ).set_flip_h( true )
		move_and_collide(Vector2(-playerMovement*delta, 0))

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
