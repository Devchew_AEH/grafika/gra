extends Area2D


var enemyMovement = 30 # to mozna zmieniac 
var enemyMovementLong = 32 * 4 # to mozna zmieniac 

onready var enemyStartPositionX = self.position.x

func _physics_process(delta): 
	move_local_x(enemyMovement * delta) 
	if (self.position.x < enemyStartPositionX): 
		get_node( "EnemyStatic" ).set_flip_h( true )
		enemyMovement = - enemyMovement 
	elif (self.position.x > enemyStartPositionX + enemyMovementLong): 
		get_node( "EnemyStatic" ).set_flip_h( false )
		enemyMovement = - enemyMovement
		
func _on_enemyDynamicLeftRight_area_entered(area): 
	get_tree().reload_current_scene()
