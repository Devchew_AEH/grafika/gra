# Gra - projekt z grafiki AEH

[zobacz stronę](https://devchew_aeh.gitlab.io/grafika/gra/)

Instrukcja w pliku [PDF](3_Gra_instrukcja.pdf)

## Narzędzia

- GODODT v3.4



## Build

- [win](https://devchew_aeh.gitlab.io/grafika/gra/build/win/game.exe)
- [web](https://devchew_aeh.gitlab.io/grafika/gra/build/web/index.html)